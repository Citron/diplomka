import numpy as np
import function
import random

upper = 5.12
lower = -5.12
pop_size = 20
dimensions = 10
iterations = 5000


def GWO():
    solutions = np.random.uniform(0, 1, (pop_size, dimensions)) * (upper - lower) + lower
    alfa = np.empty(dimensions)
    alfa_fitness = float("inf")
    beta = np.empty(dimensions)
    beta_fitness = float("inf")
    delta = np.empty(dimensions)
    delta_fitness = float("inf")

    a = 2  # parameter
    a_step = 2 / iterations

    for x in range(iterations):
        for i in range(pop_size):
            solutions[i] = np.clip(solutions[i], lower, upper)
            fitness = function.function(solutions[i])

            if fitness < alfa_fitness:
                alfa = np.copy(solutions[i])
                alfa_fitness = fitness

            if beta_fitness > fitness >= alfa_fitness:
                beta_fitness = fitness
                beta = np.copy(solutions[i])

            if delta_fitness > fitness >= beta_fitness:
                delta_fitness = fitness
                delta = np.copy(solutions[i])

            a -= a_step

        for i in range(pop_size):
            for j in range(dimensions):
                r1 = random.random()
                r2 = random.random()
                A1 = 2 * a * r1 - a  # Equation (3.3)
                C1 = 2 * r2  # Equation (3.4)

                D_alpha = abs(C1 * alfa[j] - solutions[i, j])  # Equation (3.5)-part 1
                X1 = alfa[j] - A1 * D_alpha;  # Equation (3.6)-part 1

                r1 = random.random()
                r2 = random.random()

                A2 = 2 * a * r1 - a  # Equation (3.3)
                C2 = 2 * r2  # Equation (3.4)

                D_beta = abs(C2 * beta[j] - solutions[i, j])  # Equation (3.5)-part 2
                X2 = beta[j] - A2 * D_beta  # Equation (3.6)-part 2

                r1 = random.random()
                r2 = random.random()

                A3 = 2 * a * r1 - a # Equation (3.3)
                C3 = 2 * r2  # Equation (3.4)

                D_delta = abs(C3 * delta[j] - solutions[i, j])  # Equation (3.5)-part 3
                X3 = delta[j] - A3 * D_delta  # Equation (3.5)-part 3

                solutions[i, j] = (X1 + X2 + X3) / 3  # Equation (3.7)

    return alfa, alfa_fitness
