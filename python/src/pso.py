import numpy as np
import random
import math

dimension = 2
c1 = 1.1
c2 = 1.1
w = 0.7777
bottom = -5.12
top = 5.12


class Particle:
    def __init__(self):
        self.position = np.empty([dimension])
        self.velocity = np.empty([dimension])
        self.pBest = np.empty([dimension])
        for x in range(dimension):
            self.position[x] = random.uniform(bottom, top)
            self.velocity[x] = 0
            self.pBest[x] = self.position[x]
        self.update_best_value()

    def update_position(self):
        for x in range(dimension):
            self.position[x] += self.velocity[x]

    def update_velocity(self, gBest):
        for i in range(dimension):
            r1 = random.random()
            r2 = random.random()
            social = c1 * r1 * (gBest[i] - self.position[i])
            cognitive = c2 * r2 * (self.pBest[i] - self.position[i])
            vel = (self.velocity[i]) + social + cognitive
            self.velocity[i] = vel
            print(self.position)

    def update_best_value(self):
        self.best_value = PSO.evaluate_solution(self.position)

    # nastavi pBest na pozici castice pokud je value mensi
    def update_pBest(self):
        current_value = PSO.evaluate_solution(self.position)
        if current_value < self.best_value:
            for i in range(dimension):
                self.pBest[i] = self.position[i]
            self.best_value = current_value


class PSO:
    def __init__(self, numb_of_particles):
        self.swarm = np.array([Particle() for _ in range(numb_of_particles)])
        self.solution = np.empty([dimension])
        for i in range(numb_of_particles):
            self.swarm[i] = Particle()

    @staticmethod
    def evaluate_solution(coordinates):
        suma = 0
        for i in range(len(coordinates)):
            suma += pow(coordinates[i], 2) - 10 * math.cos(2 * math.pi * coordinates[i])

        return suma + 10 * dimension

    def run_optimization(self):
        gBest = np.copy(self.swarm[0].position)
        gBest_value = PSO.evaluate_solution(gBest)

        for i in range(1000):
            print("iterace: " + str(i))
            for j in range(len(self.swarm)):
                if gBest_value > self.swarm[j].best_value:
                    gBest = np.copy(self.swarm[j].position)
                    gBest_value = self.swarm[j].best_value
                    self.solution = gBest

            for k in range(len(self.swarm)):
                self.swarm[k].update_position()
                self.swarm[k].update_velocity(gBest)
                self.swarm[k].update_pBest()
            print(self.solution)
            print(PSO.evaluate_solution(self.solution))
        return self.solution

optimizer = PSO(3)
solution = optimizer.run_optimization()
print(solution)
