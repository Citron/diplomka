package gui;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class WSATabController {

    @FXML
    private TextField eta;

    @FXML
    private TextField ro;

    private TextArea helpText;

    public void setHelpText(TextArea helpText) {
        this.helpText = helpText;
    }

    public String getEta() {
        return eta.getText();
    }

    public String getRo() {
        return ro.getText();
    }
}
