package gui;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;


public class BATTabController {

    @FXML
    private TextField r;

    @FXML
    private TextField alfa;

    @FXML
    private TextField gama;
    @FXML
    private TextField minF;
    @FXML
    private TextField maxF;
    private TextArea helpText;

    public void setHelpText(TextArea helpText) {
        this.helpText = helpText;
    }

    @FXML
    public void rHelp(MouseEvent event) {
        this.helpText.setText("");

    }

    @FXML
    public void alfaHelp(MouseEvent event) {
        this.helpText.setText("");
    }

    @FXML
    public void gamaHelp(MouseEvent event) {
        this.helpText.setText("");
    }

    @FXML
    public void minFHelp(MouseEvent event) {
        this.helpText.setText("");
    }

    @FXML
    public void maxFHelp(MouseEvent event) {
        this.helpText.setText("");
    }


    public String getR() {
        return r.getText();
    }

    public String getAlfa() {
        return alfa.getText();
    }

    public String getGama() {
        return gama.getText();
    }

    public String getMinF() {
        return minF.getText();
    }
    public String getMaxF() {
        return maxF.getText();
    }

}
