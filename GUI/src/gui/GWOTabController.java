package gui;


import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class GWOTabController {
    @FXML
    private TextField a;
    private TextArea helpText;

    public void setHelpText(TextArea helpText) {
        this.helpText = helpText;
    }

    public void aHelp(){
        helpText.setText("");
    }

    public String getA() {
        return a.getText();
    }

}
