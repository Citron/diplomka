package gui;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import swarm.*;
import swarm.boundary.Boundary;
import swarm.boundary.BoundaryType;
import swarm.boundary.SimpleBoundary;
import swarm.functions.FitnessFunction;
import swarm.functions.FunctionType;
import swarm.functions.GriewanksFunction;
import swarm.functions.RastrigFunction;

import java.io.*;


public class MainWindowController {

    @FXML
    private ChoiceBox<BoundaryType> borderType;
    @FXML
    private ChoiceBox<AlgorithmType> algorithmChoice;
    @FXML
    private ChoiceBox<FunctionType> functionTypeChoice;
    @FXML
    private TextArea helpText;
    @FXML
    private TextField dimensionSize;
    @FXML
    private CheckBox boundaryCheck;
    @FXML
    private TextField iterations;
    @FXML
    private TableColumn<String, String> dimensionColumn;
    @FXML
    private TableColumn<String, String> upperColumn;
    @FXML
    private TableColumn<String, String> lowerColumn;
    @FXML
    private TextField populationSize;
    @FXML
    private TableView<TableRow> boundaryTable;
    @FXML
    private Label resultLabel;
    @FXML
    private ImageView imageView;
    @FXML
    private AnchorPane algorithmParameters;
    @FXML
    private MenuItem saveButton;
    @FXML
    private MenuItem closeButton;
    @FXML
    private CheckBox graphCheck;

    private PSOTabController psoTabController;
    private GWOTabController gwoTabController;
    private BATTabController batTabController;
    private WSATabController wsaTabController;
    private ChartWindowController chartWindowController;

    private AnchorPane psoAnchorPane;
    private AnchorPane gwoAnchorPane;
    private AnchorPane wsaAnchorPane;
    private AnchorPane batAnchorPane;

    private int populationSizeParam;
    private int iterationParam;
    int dimensionSizeParam;
    double[] upperBoundaryParam;
    double[] lowerBoundaryParam;
    FitnessFunction functionTypeParam;
    Boundary boundaryParam;
    Result result = null;

    public void initialize() {
        try {
            FXMLLoader psoLoader = new FXMLLoader(getClass().getResource("PSOTab.fxml"));
            psoAnchorPane = psoLoader.load();
            algorithmParameters.getChildren().setAll(psoAnchorPane);
            psoTabController = psoLoader.getController();
            psoTabController.setHelpText(helpText);

            FXMLLoader gwoLoader = new FXMLLoader(getClass().getResource("GWOTab.fxml"));
            gwoAnchorPane = gwoLoader.load();
            gwoTabController = gwoLoader.getController();
            gwoTabController.setHelpText(helpText);

            FXMLLoader batLoader = new FXMLLoader(getClass().getResource("BATTab.fxml"));
            batAnchorPane = batLoader.load();
            batTabController = batLoader.getController();
            batTabController.setHelpText(helpText);

            FXMLLoader wsaLoader = new FXMLLoader(getClass().getResource("WSATab.fxml"));
            wsaAnchorPane = wsaLoader.load();
            wsaTabController = wsaLoader.getController();
            wsaTabController.setHelpText(helpText);

        } catch (IOException e) {
            System.out.println("File not found");
            e.printStackTrace();
        }

        setBorderTable();
        setAlgorithmChoiceBox();
        setFunctionType();

    }

    @FXML
    public void saveResult(ActionEvent event) {
        Stage stage = (Stage) resultLabel.getScene().getWindow();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        File file = fileChooser.showSaveDialog(stage);
        try {
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(result.getStructuredExcelOutput());
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //TODO dokon4it ukladani do souboru

    }

    @FXML
    public void closeApp(ActionEvent event) {
        Stage stage = (Stage) resultLabel.getScene().getWindow();
        stage.close();
    }


    private void setBorderTable() {
        ObservableList borders = borderType.getItems();
        borders.addAll(BoundaryType.values());
        borderType.setItems(borders);
        borderType.setValue(BoundaryType.SIMPLE);

        dimensionColumn.setCellValueFactory(new PropertyValueFactory<>("dimension"));
        upperColumn.setCellValueFactory(new PropertyValueFactory<>("upperBoundary"));
        lowerColumn.setCellValueFactory(new PropertyValueFactory<>("lowerBoundary"));

        ObservableList<TableRow> items = boundaryTable.getItems();
        boundaryTable.setItems(items);
        boundaryTable.setEditable(true);

        // Listener pro generovani radku tabulky pri změne počtu dimenzí
        dimensionSize.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if (newPropertyValue) {
                    System.out.println("Textfield on focus");
                } else {
                    items.clear();
                    int dim = Integer.parseInt(dimensionSize.getText());
                    for (int i = 1; i <= dim; i++) {
                        items.add(new TableRow(Integer.toString(i), "5.12", "-5.12"));
                    }
                }
            }
        });
        boundaryTable.setItems(items);
        upperColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        lowerColumn.setCellFactory(TextFieldTableCell.forTableColumn());
    }

    /* Sets listener for fitness function chooser and draw its equation*/
    private void setFunctionType() {
        imageView.isResizable();
        imageView.setPreserveRatio(true);
        ObservableList functions = functionTypeChoice.getItems();
        functions.addAll(FunctionType.values());
        functionTypeChoice.setItems(functions);
        functionTypeChoice.setValue(FunctionType.RASTRIG);
        imageView.setImage(FitnessFunction.getFunctionImage("f(x) =  \\sum_{i = 1}^{D} x_{i}^{2} - 10cos(2\\pi x_{i}^{2}) +10"));
        ChangeListener<FunctionType> changeListener = new ChangeListener<FunctionType>() {
            @Override
            public void changed(ObservableValue<? extends FunctionType> observable, FunctionType oldValue, FunctionType newValue) {
                String equation = "";
                Image awtImage = null;
                switch (newValue) {
                    case RASTRIG: {
                        equation = "f(x) =  \\sum_{i = 1}^{D} x_{i}^{2} - 10cos(2\\pi x_{i}^{2}) +10";
                        imageView.setFitWidth(346);
                        awtImage = FitnessFunction.getFunctionImage(equation);
                        break;
                    }
                    case GRIEWANKS: {
                        equation = "f(x) = \\sum_(i = 1)^(D) x_(i)^(2)/4000 - prod_(i = 1)^(D) cos(x_(i)/sqrt(i)) + 1";
                        imageView.setFitWidth(700);
                        awtImage = FitnessFunction.getFunctionImage(equation);
                        break;
                    }
                }

                imageView.setImage(awtImage);


            }
        };
        functionTypeChoice.getSelectionModel().selectedItemProperty().addListener(changeListener);
    }

    private void setAlgorithmChoiceBox() {

        //String[] algorithmNames = {"Particle Swarm Optimization", "Grey Wolf Optimizer", "Bat Algorithm", "Whale Algorithm"};
        ObservableList algChoices = algorithmChoice.getItems();
        algChoices.addAll(AlgorithmType.values());
        algorithmChoice.setItems(algChoices);
        algorithmChoice.setValue(AlgorithmType.PSO);

        ChangeListener<AlgorithmType> changeListener = new ChangeListener<AlgorithmType>() {
            @Override
            public void changed(ObservableValue<? extends AlgorithmType> observable, AlgorithmType oldValue, AlgorithmType newValue) {
                switch (newValue) {
                    case PSO: {
                        algorithmParameters.getChildren().setAll(psoAnchorPane);
                        break;
                    }
                    case BAT: {
                        algorithmParameters.getChildren().setAll(batAnchorPane);
                        break;
                    }
                    case GWO: {
                        algorithmParameters.getChildren().setAll(gwoAnchorPane);
                        break;
                    }
                    case WSA: {
                        algorithmParameters.getChildren().setAll(wsaAnchorPane);
                        break;
                    }
                }
            }
        };
        algorithmChoice.getSelectionModel().selectedItemProperty().addListener(changeListener);

    }

    /*Sets all boundary values of boundary table to passed value */
    private void setTableToSameValues(String value) {
        ObservableList<TableRow> rows = boundaryTable.getItems();
        for (TableRow row : rows) {
            row.setUpperBoundary(value);
            row.setLowerBoundary(value);
        }
        boundaryTable.setItems(rows);
    }


    /*Editing of lower boundary value in table by double click*/
    @FXML
    void lowerBoundEdit(TableColumn.CellEditEvent<TableRow, String> event) {
        TableRow row = boundaryTable.getSelectionModel().getSelectedItem();
        row.setLowerBoundary(event.getNewValue());
        if (boundaryCheck.isSelected()) {
            setTableToSameValues(row.getLowerBoundary());
        }
    }

    /*Editing upper boundary value in boundary table by double click*/
    @FXML
    void upperBoundEdit(TableColumn.CellEditEvent<TableRow, String> event) {
        TableRow row = boundaryTable.getSelectionModel().getSelectedItem();
        row.setUpperBoundary(event.getNewValue());
        if (boundaryCheck.isSelected()) {
            setTableToSameValues(row.getUpperBoundary());
        }
    }

    /*Runs selected algorithm */
    @FXML
    void run(ActionEvent event) {

        try {
            parseUserInput();
            AlgorithmType alg = algorithmChoice.getValue();
            switch (alg) {
                case PSO: {
                    PSO pso = getPSOAlgorithm();
                    result = pso.runAlgorithm();
                    resultLabel.setText(result.getStructuredOutput());
                    break;
                }
                case BAT: {
                    BATAlg batAlg = getBATAlgorithm();
                    result = batAlg.runAlgorithm();
                    resultLabel.setText(result.getStructuredOutput());
                    break;
                }
                case GWO: {
                    GWO gwo = getGWOAlgorithm();
                    result = gwo.runAlgorithm();
                    resultLabel.setText(result.getStructuredOutput());
                    break;
                }
                case WSA: {
                    WSA wsa = getWSAAlgorithm();
                    result = wsa.runAlgorithm();
                    resultLabel.setText(result.getStructuredOutput());
                    break;
                }
            }
            FXMLLoader chartWindowLoader = new FXMLLoader(getClass().getResource("ChartWindow.fxml"));
            try {
                if (graphCheck.isSelected()) {
                    Parent root = chartWindowLoader.load();
                    chartWindowController = chartWindowLoader.getController();
                    chartWindowController.setData(result.getConvergence());
                    Stage stage = new Stage();
                    stage.setTitle("My New Stage Title");
                    stage.setScene(new Scene(root, 450, 450));
                    stage.showAndWait();
                    //stage.show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Je nutné správně zadat všechny parametry algoritmu.");
            alert.setHeaderText("Chyba");
            alert.showAndWait();
        }
    }

    //Vytvoreni zvolene fitness funkce pro vybrany algoritmus
    private FitnessFunction getSelectedFitnessFunction(int dimensionSize) {
        FunctionType functionType = functionTypeChoice.getValue();
        switch (functionType) {
            case RASTRIG: {
                return new RastrigFunction(dimensionSize);
            }
            case GRIEWANKS: {
                return new GriewanksFunction(dimensionSize);
            }
            default:
                return null;
        }
    }

    private Boundary getSelectedBoudary() {
        BoundaryType boundaryType = borderType.getValue();
        switch (boundaryType) {
            case SIMPLE: {
                return new SimpleBoundary();
            }
            default: {
                return null;
            }
        }
    }

    private void parseUserInput() {
        populationSizeParam = Integer.parseInt(populationSize.getText());
        iterationParam = Integer.parseInt(iterations.getText());
        dimensionSizeParam = Integer.parseInt(dimensionSize.getText());
        upperBoundaryParam = new double[dimensionSizeParam];
        lowerBoundaryParam = new double[dimensionSizeParam];
        ObservableList<TableRow> items = boundaryTable.getItems();
        for (int i = 0; i < dimensionSizeParam; i++) {
            upperBoundaryParam[i] = Double.parseDouble((items.get(i)).upperBoundaryProperty().getValue());
            lowerBoundaryParam[i] = Double.parseDouble((items.get(i)).lowerBoundaryProperty().getValue());
        }
        functionTypeParam = getSelectedFitnessFunction(dimensionSizeParam);
        boundaryParam = getSelectedBoudary();
    }

    /*Returns WSA algorithm instance from parsed user input*/
    private WSA getWSAAlgorithm() {
        float ro = Float.parseFloat(wsaTabController.getRo());
        float eta = Float.parseFloat(wsaTabController.getEta());
        WSA wsa = new WSA(populationSizeParam, dimensionSizeParam, upperBoundaryParam, lowerBoundaryParam, iterationParam, functionTypeParam, boundaryParam, ro, eta);
        return wsa;
    }

    /*Returns BAT algorithm instance from parsed user input*/
    private BATAlg getBATAlgorithm() {
        float alfa = Float.parseFloat(batTabController.getAlfa());
        float gama = Float.parseFloat(batTabController.getGama());
        float r = Float.parseFloat(batTabController.getR());
        float minF = Float.parseFloat(batTabController.getMinF());
        float maxF = Float.parseFloat(batTabController.getMaxF());
        BATAlg batAlg = new BATAlg(populationSizeParam, dimensionSizeParam, upperBoundaryParam, lowerBoundaryParam, iterationParam, functionTypeParam, boundaryParam, minF, maxF, r, alfa, gama);
        return batAlg;
    }

    /*Returns WSA algorithm instance from parsed user input*/
    private GWO getGWOAlgorithm() {
        float a = Float.parseFloat(gwoTabController.getA());
        GWO gwo = new GWO(populationSizeParam, dimensionSizeParam, upperBoundaryParam, lowerBoundaryParam, iterationParam, functionTypeParam, boundaryParam, a);
        return gwo;
    }

    /*Returns PSO algorithm instance from parsed user input*/
    private PSO getPSOAlgorithm() {
        double c1 = Double.parseDouble(psoTabController.getC1Value());
        double c2 = Double.parseDouble(psoTabController.getC2Value());
        double w = Double.parseDouble(psoTabController.getWValue());
        PSO pso = new PSO(populationSizeParam, dimensionSizeParam, upperBoundaryParam, lowerBoundaryParam, iterationParam, functionTypeParam, boundaryParam, c1, c2, w);
        return pso;
    }

    @FXML
    void setBoundaryTable(ActionEvent event) {
        ObservableList items = boundaryTable.getItems();
        items.add("asdad");
        boundaryTable.setItems(items);
    }

    @FXML
    void chooseFunction(MouseEvent event) {
    }

    @FXML
    void helpPopulationSize(MouseEvent event) {
        helpText.setText("Zadej velikost populace algoritmu.\nVelikost populace musí být kladné celé číslo větší než 2.");
    }

    @FXML
    void helpIterations(MouseEvent event) {
        helpText.setText("Zadej počet iterací, pro běh algoritmu.\nZadej celé kladné číslo. ");
    }

    @FXML
    void helpDimensions(MouseEvent event) {
        helpText.setText("Zadej počet dimenzí optimalizované funkce.\nZadej celé kladné číslo.");
    }

    @FXML
    void helpFunctionSelected(MouseEvent event) {
        helpText.setText("Vyber funkci pro výpočet fitness hodoty.");
    }

    @FXML
    void helpBorderType(MouseEvent event) {
        helpText.setText("Vyber typ hranice prohledávaného prostoru.");
    }

    @FXML
    void helpC1(MouseEvent event) {
        helpText.setText("Zadej učící parametr C1 pro algoritmus.\nParametr musí být kladné číslo.");
    }


    public TextArea getHelpTextField() {
        return this.helpText;
    }
}
