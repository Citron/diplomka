package swarm.boundary;

public class SimpleBoundary implements Boundary {
    public double[] moveCoordinatesIntoBoundary(double[] coordinates, double[] upperBoundary, double[] lowerBoundary) {
        for (int i = 0; i < coordinates.length; i++) {
            if (coordinates[i] < lowerBoundary[i]) {
                coordinates[i] = lowerBoundary[i];
            }
            if (coordinates[i] > upperBoundary[i]) {
                coordinates[i] = upperBoundary[i];
            }
        }
        return coordinates;
    }
}
