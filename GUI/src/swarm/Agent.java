package swarm;

import java.util.Arrays;

public abstract class Agent {
    private double[] position;
    private double fitnessValue;

    public Agent(double[] position, double fitnessValue) {
        this.position = position;
        this.fitnessValue = fitnessValue;
    }

    public Agent() {
    }

    public void setPosition(double[] position) {
        this.position = position.clone();
    }

    public double[] getPosition() {
        return position.clone();
    }

    public double getFitnessValue() {
        return fitnessValue;
    }

    public void setFitnessValue(double fitnessValue) {
        this.fitnessValue = fitnessValue;
    }

    @Override
    public String toString() {
        return "Agent{" +
                "position=" + Arrays.toString(position) +
                ", fitnessValue=" + fitnessValue +
                '}';
    }

    public String getCoordinates() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < position.length; i++) {
            if(i == position.length-1){
                sb.append(Math.round(position[i] * 100.0) / 100.0 );
            }
            sb.append(Math.round(position[i] * 100.0) / 100.0 + ", ");
        }
        sb.append("]");
        return sb.toString();
    }
}
