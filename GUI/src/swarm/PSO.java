package swarm;

import swarm.boundary.Boundary;
import swarm.functions.FitnessFunction;

import java.util.ArrayList;
import java.util.Random;

public class PSO implements SwarmAlgorithm {
    private final double w;
    private final double c1;
    private final double c2;
    private final Boundary boundary;
    private final int populationSize;
    private final int dimensionSize;
    private ArrayList<Particle> population = new ArrayList<Particle>();
    private Particle gBest = new Particle();
    private final double[] upperBoundaries;
    private final double[] lowerBoundaries;
    private final int iterationNumber;
    private final FitnessFunction function;
    private final Random random = new Random();

    public PSO(int populationSize, int dimensionSize, double[] upperBoundaries, double[] lowerBoundaries,
               int iterationNumber, FitnessFunction function, Boundary boundary, double c1, double c2, double w) {


        if (populationSize < 1) {
            throw new IllegalArgumentException("Invalid population value: " + populationSize);
        }
        if (dimensionSize < 2) {
            throw new IllegalArgumentException("Invalid dimension size value: " + dimensionSize);
        }
        if (lowerBoundaries == null || lowerBoundaries.length < dimensionSize) {
            throw new IllegalArgumentException("Invalid lower boundaries value");
        }
        if (upperBoundaries == null || upperBoundaries.length < dimensionSize) {
            throw new IllegalArgumentException("Invalid upper boundaries value");
        }
        if (iterationNumber < 1) {
            throw new IllegalArgumentException("Invalid number of iteration: " + iterationNumber);
        }
        if (function == null) {
            throw new IllegalArgumentException("Invalid fitness function");
        }
        if (boundary == null) {
            throw new IllegalArgumentException("Invalid boundary");
        }
        this.function = function;
        this.populationSize = populationSize;
        this.dimensionSize = dimensionSize;
        this.lowerBoundaries = lowerBoundaries.clone();
        this.upperBoundaries = upperBoundaries.clone();
        this.iterationNumber = iterationNumber;
        this.w = w;
        this.c1 = c1;
        this.c2 = c2;
        this.boundary = boundary;
    }

    public void initializePopulation() {
        createPopulation(this.populationSize);
        double[] position = new double[this.getDimensionSize()];
        double[] velocity = new double[this.getDimensionSize()];
        for (Particle particle : population) {
            for (int i = 0; i < getDimensionSize(); i++) {
                position[i] = this.getLowerBoundaries()[i] + (this.getUpperBoundaries()[i] - this.getLowerBoundaries()[i]) *
                        this.getRandom().nextDouble();
                velocity[i] = 0;
            }
            particle.setPosition(position);
            particle.setVelocity(velocity);
            particle.setFitnessValue(function.getFitness(particle.getPosition()));
            particle.setPBestPosition(position);
            particle.setPBestValue(this.getFunction().getFitness(position));
        }
    }

    private void createPopulation(int popSize) {
        for (int i = 0; i < popSize; i++) {
            this.population.add(new Particle());
        }
    }


    //updates velocity of particle with given index
    private void updateVelocity(Particle particle) {
        double r1, r2, social, cognitive;
        double[] velocity = new double[getDimensionSize()];
        for (int i = 0; i < getDimensionSize(); i++) {
            r1 = getRandom().nextDouble();
            r2 = getRandom().nextDouble();
            social = c1 * r1 * (this.getgBest().getPosition()[i] - particle.getPosition()[i]);
            cognitive = c2 * r2 * (particle.getPBestPosition()[i] - particle.getPosition()[i]);
            velocity[i] = particle.getVelocity()[i] + social + cognitive;
        }
        particle.setVelocity(velocity);

    }

    //updates positions for given particle index
    public void updatePosition(Particle particle) {
        double[] newPossition = new double[getDimensionSize()];
        for (int i = 0; i < getDimensionSize(); i++) {
            newPossition[i] = particle.getPBestPosition()[i] + particle.getVelocity()[i];
        }
        particle.setPosition(boundary.moveCoordinatesIntoBoundary(newPossition, getUpperBoundaries(), getLowerBoundaries()));
    }

    public Result runAlgorithm() {
        this.initializePopulation();
        this.initializeAlgorithm();
        long startTime = System.currentTimeMillis();
        double[] convergence = new double[this.getIterationNumber()];
        for (int i = 0; i < this.getIterationNumber(); i++) {
            //finding of gBest
            for (Particle particle : getPopulation()) {
                if (this.getgBest().getFitnessValue() > particle.getFitnessValue()) {
                    this.setgBest(particle);
                }
            }
            //actualization of particle positions
            for (Particle particle : this.getPopulation()) {
                {
                    this.updatePosition(particle);
                    this.updateVelocity(particle);
                    particle.setFitnessValue(function.getFitness(particle.getPosition()));
                    particle.updatePBest();
                }
            }
            //TODO vyresit opcet iteraci, budou se pocitat?
            System.out.println("Iterace " + i + "  " + gBest.toString());
            convergence[i] = gBest.getFitnessValue();
        }
        long endTime = System.currentTimeMillis();
        return new Result(getgBest(), getIterationNumber(), startTime, endTime, convergence, getFunction().getName(), AlgorithmType.PSO, getC1(), getC2(), getW());
    }

    public void setgBest(Particle gBest) {
        this.gBest.setPBestValue(gBest.getPBestValue());
        this.gBest.setPosition(gBest.getPosition());
        this.gBest.setFitnessValue(gBest.getFitnessValue());
        this.gBest.setVelocity(gBest.getVelocity());
        this.gBest.setPBestPosition(gBest.getPBestPosition());
    }

    public void updateGBest() {
        for (Particle particle : getPopulation()) {
            if (gBest.getFitnessValue() > particle.getFitnessValue()) {
                setgBest(particle);
            }
        }
    }


    //initiation of position, fitness and velocity
    public void initializeAlgorithm() {
        for (Particle particle : this.getPopulation()) {
            particle.setPBestPosition(particle.getPosition());
            particle.updatePBest();
        }
        getgBest().setFitnessValue(Double.MAX_VALUE);
    }


    public double getW() {
        return w;
    }

    public double getC1() {
        return c1;
    }

    public double getC2() {
        return c2;
    }

    public Boundary getBoundary() {
        return boundary;
    }

    public int getPopulationSize() {
        return populationSize;
    }

    public int getDimensionSize() {
        return dimensionSize;
    }

    public ArrayList<Particle> getPopulation() {
        return population;
    }

    public void setPopulation(ArrayList<Particle> population) {
        this.population = population;
    }

    public Particle getgBest() {
        return gBest;
    }

    public double[] getUpperBoundaries() {
        return upperBoundaries;
    }

    public double[] getLowerBoundaries() {
        return lowerBoundaries;
    }

    public int getIterationNumber() {
        return iterationNumber;
    }

    public FitnessFunction getFunction() {
        return function;
    }

    public Random getRandom() {
        return random;
    }
}
