package swarm;

public class Particle extends Agent {
    private double[] velocity;
    private double[] pBestPosition;
    private double pBestValue = Double.MAX_VALUE;

    public void updatePBest() {
        if (this.getPBestValue() > this.getFitnessValue()) {
            setPBestValue(this.getFitnessValue());
            this.setPBestPosition(this.getPosition());
        }
    }

    public double[] getVelocity() {
        return velocity.clone();
    }

    public void setVelocity(double[] velocity) {
        this.velocity = velocity.clone();
    }

    public double[] getPBestPosition() {
        return pBestPosition.clone();
    }

    public void setPBestPosition(double[] pBestPosition) {
        this.pBestPosition = pBestPosition.clone();
    }

    public double getPBestValue() {
        return pBestValue;
    }

    public void setPBestValue(double pBestValue) {
        this.pBestValue = pBestValue;
    }
}
