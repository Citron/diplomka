package swarm;

public enum AlgorithmType {
    PSO("Particle Swarm Optimization"),
    GWO("Grey Wolf Optimizer"),
    BAT("Bat Algorithm"),
    WSA("Whale Swarm Algorithm");

    private final String text;

    AlgorithmType(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return this.text;
    }
}
