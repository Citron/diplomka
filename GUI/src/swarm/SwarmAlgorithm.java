package swarm;

public interface SwarmAlgorithm {
    Result runAlgorithm();
    void initializePopulation();

}
