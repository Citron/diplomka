package swarm.functions;

public class HappyCatFunction extends FitnessFunction {

    public HappyCatFunction(double dimensionSize) {
        super(dimensionSize);
    }

    @Override
    public double getFitness(double[] position) {
        double sum = 0;
        for (int i = 0; i < position.length; i++) {
            sum = sum + Math.pow(position[i], 2);
        }
        sum = sum - position.length;
        sum = Math.abs(Math.pow(sum, 1 / 4));

        double sum2 = 0;
        double sum1 = 0;
        for (int i = 0; i < position.length; i++) {
            sum2 = sum2 + Math.pow(position[i], 2);
            sum1 = sum1 + sum1;
        }

        double secondBraket = (0.5 * sum2 + sum1) / position.length;
        return sum + secondBraket + 0.5;
    }

    @Override
    public String getName() {
        return null;
    }
}
